/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Strangexz
 */
public class ConnParam {
        private String url;
	private String driver;
	private String user;
	private String password;
	private String tableName;
	private String lib;
        
        public ConnParam(){
            
        }
        
        public ConnParam(String url, String driver, String user, String password, String tableName, String lib) {
		super();
		this.url = url;
		this.driver = driver;
		this.user = user;
		this.password = password;
		this.tableName = tableName;
		this.lib = lib;
	}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getLib() {
        return lib;
    }

    public void setLib(String lib) {
        this.lib = lib;
    }
        
        
        
        @Override
	public String toString() {
		return "\n-----Base de Datos:-----"+
                "Driver["+driver+"]"+                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                "Url["+url+"]"+
                "Usuario["+user+"]"+
                "Contraseña"+password+"]"+
                "Nombre de Tabla["+tableName+"]"+
                "Libreria["+lib+"]";
	}
    
}
