/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.io.FileInputStream;
import java.sql.Blob;

/**
 *
 * @author Strangexz
 */
public class Archivo {
    private String nombre;
    private String extension;
    private String tamaño;
    private String ruta;
    private FileInputStream data;
    private String diaCreacion;
    private String mesCreacion;
    private String añoCreacion;
    private String horaCreacion;
    private String usuarioCreador;
    private Blob xml;
    
    public Archivo(){
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getDiaCreacion() {
        return diaCreacion;
    }

    public void setDiaCreacion(String diaCreacion) {
        this.diaCreacion = diaCreacion;
    }

    public String getMesCreacion() {
        return mesCreacion;
    }

    public void setMesCreacion(String mesCreacion) {
        this.mesCreacion = mesCreacion;
    }

    public String getAñoCreacion() {
        return añoCreacion;
    }

    public void setAñoCreacion(String añoCreacion) {
        this.añoCreacion = añoCreacion;
    }

    public String getHoraCreacion() {
        return horaCreacion;
    }

    public void setHoraCreacion(String horaCreacion) {
        this.horaCreacion = horaCreacion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public FileInputStream getData() {
        return data;
    }

    public void setData(FileInputStream data) {
        this.data = data;
    }

    public Blob getXml() {
        return xml;
    }

    public void setXml(Blob xml) {
        this.xml = xml;
    }
    
    
    
    

    
    
    @Override
    public String toString(){
        return "Nombre del archivo: ["+nombre+
                "]\nExtension del archivo: ["+extension+                
                "]\nTamaño del archivo: ["+tamaño+
                "]\nRuta del archivo: ["+ruta+
                "]\nDatos del Archivo: ["+data+
                "]\nFecha de creación: ["+horaCreacion+
                "]\nFecha de creación: ["+diaCreacion+
                "]\nFecha de creación: ["+mesCreacion+
                "]\nFecha de creación: ["+añoCreacion+
                "]\nArchivo XML: ["+xml+
                "]\nUsuario Creador: ["+usuarioCreador+"]";
    }

  
    
    
}
