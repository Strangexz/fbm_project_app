/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Strangexz
 */
public class Usuario {
    private String user;
    private String contraseña;
    private String horaCreacion;
    private String diaCreacion;
    private String mesCreacion;
    private String añoCreacion;
    private String nombre;
    private String rol;
    private String horaCambio;
    private String diaCambio;
    private String mesCambio;
    private String añoCambio;
    private String usuarioCambio;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getHoraCreacion() {
        return horaCreacion;
    }

    public void setHoraCreacion(String horaCreacion) {
        this.horaCreacion = horaCreacion;
    }

    public String getDiaCreacion() {
        return diaCreacion;
    }

    public void setDiaCreacion(String diaCreacion) {
        this.diaCreacion = diaCreacion;
    }

    public String getMesCreacion() {
        return mesCreacion;
    }

    public void setMesCreacion(String mesCreacion) {
        this.mesCreacion = mesCreacion;
    }

    public String getAñoCreacion() {
        return añoCreacion;
    }

    public void setAñoCreacion(String añoCreacion) {
        this.añoCreacion = añoCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getHoraCambio() {
        return horaCambio;
    }

    public void setHoraCambio(String horaCambio) {
        this.horaCambio = horaCambio;
    }

    public String getDiaCambio() {
        return diaCambio;
    }

    public void setDiaCambio(String diaCambio) {
        this.diaCambio = diaCambio;
    }

    public String getMesCambio() {
        return mesCambio;
    }

    public void setMesCambio(String mesCambio) {
        this.mesCambio = mesCambio;
    }

    public String getAñoCambio() {
        return añoCambio;
    }

    public void setAñoCambio(String añoCambio) {
        this.añoCambio = añoCambio;
    }

    public String getUsuarioCambio() {
        return usuarioCambio;
    }

    public void setUsuarioCambio(String usuarioCambio) {
        this.usuarioCambio = usuarioCambio;
    }
    
    
    
     public String toString(){
        return "Nick del Usuario: ["+user+
                "]\nContraseña de usuario: ["+contraseña+                
                "]\nHora de la creación del usuario: ["+horaCreacion+
                "]\nDia de la creación del usuario: ["+diaCreacion+                                
                "]\nMes de la creación del usuario: ["+mesCreacion+
                "]\nAño de la creación del usuario: ["+añoCreacion+
                "]\nNombre del Usuario: ["+nombre+"]"+
                "]\nHora de la modificación: ["+horaCambio+
                "]\nDia de la modificación: ["+diaCambio+
                "]\nMes de la modificación: ["+mesCambio+
                "]\nAño de la modificación: ["+añoCambio+
                "]\nUsuario que realizó la modificación: ["+usuarioCambio+"]";
    }
    
    
}
