/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.io.Serializable;

/**
 *
 * @author Strangexz
 */
public class Binario implements Serializable {
    private static final long serialVersionUID = 666L;
    private String nombre;
    private String tamaño;
    
    public Binario (String nombre){
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }
    
    @Override
    public String toString(){
        return "Nombre del archivo: "+nombre+
                " Tamaño del archivo: "+tamaño;
    }

    
    
    
}
