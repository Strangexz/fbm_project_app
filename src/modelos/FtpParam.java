/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Strangexz
 */
public class FtpParam {
    private String url;
    private String user;
    private String password;
    private String port;
    private String path;
    private String server;
    
    public FtpParam(){
         
    }
        
    public FtpParam(String url, String user, String password, String port, String path) {
	super();
	this.url = url;
        this.user = user;
        this.password = password;
        this.port = port;
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }
    
    
    
    
    
    @Override
	public String toString() {
		return "\n-----Servidor FTP-----"+
                "Url["+url+"]"+
                "Usuario["+user+"]"+
                "Contraseña["+password+"]"+
                "Puerto["+port+"]"+
                "Direccion["+path+"]";
	}
    
}
