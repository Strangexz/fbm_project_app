/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connections;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelos.Archivo;
import modelos.FtpParam;
import org.apache.commons.net.ftp.FTP;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import procesos.AdminSystem;
import static procesos.AdminSystem.registroBitacora;
import utils.LogManager;

/**
 *
 * @author Wing Zero
 */
public class FtpManager {
    //Cliente de conexión a FTP
    private static FTPClient ftp = new FTPClient();
    
    //Comprobamos estado de la conexión
    public static boolean conectado = false;
        
	
    //Configurando el log desde la clase FTPParam
//	static private Logger log = Logger.getLogger(FtpManager.class);
    private static Logger log = LogManager.initLogger(FtpManager.class);
	
    //Lista de los archivos
    static private String[] listaArchivos;
        
        public FtpManager(FtpParam ftpCon){
            log.debug(ftpCon);
            openConectionFTP(ftpCon);
        }
		
	public static int openConectionFTP(FtpParam ftpClient){
		
		int respuesta = 0;
		
		try{
			log.info("CONECTANDOSE AL SERVIDOR FTP");
			//Conectandose e identificandose con el servidor FTP
			ftp.connect(ftpClient.getUrl());
			
			log.info("IDENTIFICANDOSE CON EL SERVIDOR");
			ftp.login(ftpClient.getUser(), ftpClient.getPassword());
			
			//Entrando en modo pasivo
			log.info("ENTRANDO EN MODO PASIVO");
			ftp.enterLocalPassiveMode();
//                        ftp.enterLocalActiveMode();
			
			//obteniendo respuesta del servidor
			respuesta = ftp.getReplyCode();
			log.info("RESPUESTA :"+respuesta);			
			
			//Si la respuesta del servidor indica que podemos pasar, procedemos a mostrar
			//el directorio al que estamos conectados
			log.info("CONEXIÓN EXITOSA");            
                        JOptionPane.showMessageDialog(null, "¡¡¡CONEXIÓN ÉXITOSA!!!", "INFO", JOptionPane.INFORMATION_MESSAGE);
                        conectado = true;
			log.info("CONECTADO AL DIRECTORIO: "+ftp.printWorkingDirectory());
			
			//Cambiamos de directorio
//			ftp.changeWorkingDirectory(ftpClient.getPath());
                        ftp.changeWorkingDirectory("/30911324");
			log.info("REUBICADO AL DIRECTORIO: "+ftp.printWorkingDirectory());
			
			//A continuación listamos los archivos de dicho directorio
			if(FTPReply.isPositiveCompletion(respuesta) == true){
				log.info("LISTANDO ARCHIVOS");
				listaArchivos = ftp.listNames();
				
				for(int i=0; i<listaArchivos.length;i++){
					log.debug(listaArchivos[i]);
					
				}//fin del for
				
			//Si no, avisamos				
			}else{
				log.warn("¡ERROR AL DESPLEGAR LA LISTA DE ARCHIVOS!");
                                JOptionPane.showMessageDialog(null, "¡¡¡ERROR AL DESPLEGAR LA LISTA DE ARCHIVOS!!!", "AVISO", JOptionPane.WARNING_MESSAGE);
			}//fin del else
			
		}catch(Exception e){
			log.error("¡ERROR DE CONEXIÓN!: "+e);
                        JOptionPane.showMessageDialog(null, "¡¡¡ERROR DE CONEXIÓN AL SERVIDOR FTP!!!", "ERROR", JOptionPane.ERROR_MESSAGE);
                        conectado = false;
		}//fin del catch
                
                return respuesta;
		
	}
	
	public static void closeConectionFTP(){
		
		try{
                    //Cerramos sesión y nos desconectamos
                    JOptionPane.showMessageDialog(null, "¡¡¡CERRANDO SESIÓN!!!", "INFO", JOptionPane.INFORMATION_MESSAGE);
                    log.info("CERRANDO SESIÓN");
                    ftp.logout();
                    log.info("DESCONECTANDOSE DEL SERVIDOR");
                    JOptionPane.showMessageDialog(null, "¡¡¡DESCONECTANDOSE DEL SERVIDOR!!!", "INFO", JOptionPane.INFORMATION_MESSAGE);
                    ftp.disconnect();
		//En caso de no poder desconectarnos y/o cerrar sesión
		}catch(Exception e){
			log.warn(e.getMessage()+"\n¡ERROR AL DECOSCONECTARSE Y/O CERRAR SESIÓN!");
                        JOptionPane.showMessageDialog(null, "¡¡¡ERROR AL  DECOSCONECTARSE Y/O CERRAR SESIÓN!!!", "AVISO", JOptionPane.WARNING_MESSAGE);
			
		}//fin del catch
		
	}
	
	public static void uploadFileFTP(String pathfile, String pathhost){
		String nameFile;
		
		//Substraemos del path el nombre y extensión del archivo
		nameFile = pathfile.substring(pathfile.lastIndexOf("\\")+1);
//		log.debug("nameFile = "+nameFile);
//		log.debug("pathhost = "+pathhost );
		
		try{
			//Activamos el envio de cualquier archivo
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			BufferedInputStream buffIn = null;
			
			//Asignamos la ruta en donde deseamos subir el archivo
			ftp.changeWorkingDirectory(pathhost);
			
			//Confirmamos la ruta local del archivo a enviar
			buffIn = new BufferedInputStream(new FileInputStream(pathfile));
			ftp.enterLocalPassiveMode();
			
			//Confirmamos el nombre y extensión del archivo a subir
			ftp.storeFile(nameFile, buffIn);
			log.info("¡ARCHIVO SUBIDO SATISFACTORIAMENTE!");
                        JOptionPane.showMessageDialog(null, "¡ARCHIVO SUBIDO SATISFACTORIAMENTE!", "INFO", JOptionPane.INFORMATION_MESSAGE);
			
			//Mostramos la lista de archivos actualizada
			log.info("ACTUALIZANDO LISTA DE ARCHIVOS");
                        JOptionPane.showMessageDialog(null, "ACTUALIZANDO LISTA DE ARCHIVOS", "INFO", JOptionPane.INFORMATION_MESSAGE);
			listaArchivos = ftp.listNames();
			for(int i=0; i<listaArchivos.length;i++){
				log.debug(listaArchivos[i]);
				
			}//fin del for
			
			//Cerramos el envio de archivos al FTP
			buffIn.close(); 
		}catch(Exception e){
			log.error(e.getMessage()+"\n¡NO SE PUDO CARGAR EL ARCHIVO!");
                        JOptionPane.showConfirmDialog(null, "¡NO SE PUDO CARGAR EL ARCHIVO!", "ERROR", JOptionPane.ERROR_MESSAGE);
		}//fin del catch
		
		
	}
        
        public static void uploadFileFTP(File file, String nombre, String pathhost){
		String nameFile;
		
		//Substraemos del path el nombre y extensión del archivo
		nameFile = nombre;
//		log.debug("nameFile = "+nameFile);
//		log.debug("pathhost = "+pathhost );
		
		try{
			//Activamos el envio de cualquier archivo
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			BufferedInputStream buffIn = null;
			
			//Asignamos la ruta en donde deseamos subir el archivo
			ftp.changeWorkingDirectory(pathhost);
			
			//Confirmamos la ruta local del archivo a enviar
			buffIn = new BufferedInputStream(new FileInputStream(file));
			ftp.enterLocalPassiveMode();
			
			//Confirmamos el nombre y extensión del archivo a subir
			ftp.storeFile(nameFile, buffIn);
			log.info("¡ARCHIVO SUBIDO SATISFACTORIAMENTE!");
                        JOptionPane.showMessageDialog(null, "¡ARCHIVO SUBIDO SATISFACTORIAMENTE!", "INFO", JOptionPane.INFORMATION_MESSAGE);
			
			//Mostramos la lista de archivos actualizada
			log.info("ACTUALIZANDO LISTA DE ARCHIVOS");
                        JOptionPane.showMessageDialog(null, "ACTUALIZANDO LISTA DE ARCHIVOS", "INFO", JOptionPane.INFORMATION_MESSAGE);
			listaArchivos = ftp.listNames();
			for(int i=0; i<listaArchivos.length;i++){
				log.debug(listaArchivos[i]);
				
			}//fin del for
			
			//Cerramos el envio de archivos al FTP
			buffIn.close(); 
                        
                        registroBitacora("ADD_FTP", "El usuario ["+AdminSystem.usuarioLogeado()+"] subio el archivo ["+nombre+"] al servidor FTP");
		}catch(Exception e){
			log.error(e.getMessage()+"\n¡NO SE PUDO CARGAR EL ARCHIVO!");
                        JOptionPane.showMessageDialog(null, "¡NO SE PUDO CARGAR EL ARCHIVO!", "ERROR", JOptionPane.ERROR_MESSAGE);
		}//fin del catch
		
		
	}
        
	
	public static void downloadFileFTP(){
		
		String archivoSelec = "", pathFile = "";
		
		//Nos aseguramos de que la lista de los nombres de archivos no este vacia
		if(listaArchivos != null && listaArchivos.length > 0){
			
			try{
				//Actualizamos la lista
				log.info("RECUPERANDO LISTA DE ARCHIVOS ACTUALIZADA");
				listaArchivos = ftp.listNames();
			//En caso de no poder actualizar la lista de archivos
			}catch(Exception e){
				log.warn(e+" NO SE PUDO ACTUALIZAR LA LISTA DE ARCHIVOS");
			}//fin del catch
			
			//Cargamos la lista de nombres en el Dialog
			JFrame frame = new JFrame("FTP Conection Manager");
			archivoSelec = (String) JOptionPane.showInputDialog(frame, "Que Archivo desea bajar?","Archivos en el FTP",JOptionPane.QUESTION_MESSAGE,null, listaArchivos,listaArchivos[0]);
			
			JFileChooser guardar = new JFileChooser();
			/*
			 * Ojo en la siguente línea le asignamos el nombre del archivo al textField del saveDialog
			 */
			guardar.setSelectedFile(new File(archivoSelec));
			//Seleccionamos la ruta en la que guardaremos el archivo a descargar
			guardar.showSaveDialog(null);
			pathFile = guardar.getCurrentDirectory().getAbsolutePath();
//			System.out.println("pathFile: "+pathFile);
			
			try{
				//Asignamos la ruta local y nombre del archivo con su extensión y lo descargamos
				FileOutputStream output = new FileOutputStream(pathFile+"/"+archivoSelec);
				ftp.retrieveFile(archivoSelec, output);
				log.info("ARCHIVO DESCARGADO EXITOSAMENTE");
				//Cerramos la descarga de archivos
				output.close();
			//En caso de que la descarga no se pudo realizar	
			}catch(Exception e){
				 log.error(e+" NO SE PUDO REALIZAR LA DESCARGA");
			}//fin del catch
		}//fin del if
		
	}

    public static boolean isConectado() {
        return conectado;
    }

    public static void setConectado(boolean conectado) {
        FtpManager.conectado = conectado;
    }
        
        
    
}
