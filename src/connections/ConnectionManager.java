/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connections;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import modelos.ConnParam;
import org.apache.log4j.Logger;
import utils.LogManager;

/**
 *
 * @author Strangexz
 */
public class ConnectionManager {
//    private Logger log =  Logger.getLogger(ConnectionManager.class);
    private static Logger log = LogManager.initLogger(ConnectionManager.class);
    private ConnParam conPar = null;
    private Connection  conn = null;
    
    public ConnectionManager(ConnParam conPar){
        this.conPar = conPar;
        conect();
    }
    
    public void conect(){
        try{
            log.info("Estableciendo conexión JDBC a [" + conPar.getUrl()+conPar.getLib() +" " + conPar.getDriver() + "]");
            DriverManager.registerDriver((Driver) Class.forName(conPar.getDriver()).newInstance());
            conn = DriverManager.getConnection(conPar.getUrl()+conPar.getLib(), conPar.getUser(), conPar.getPassword());         
            
            if(conn != null){
                log.info("CONEXIÓN EXITOSA!!!!");
            }
            
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
	} catch (Exception e) {
            e.printStackTrace();
	}
    }
    public PreparedStatement createPreparedStatement(String query) {
	try {
		return conn.prepareStatement(query);
	} catch (Exception e) {
		e.printStackTrace();
	} return null;
    }

    public Statement createStatement() {
    	try {
            return conn.createStatement();
	} catch (Exception e) {
            e.printStackTrace();
	} return null;
    }

    public void close() {
        log.info("Cerrando conexiÃ³n JDBC [" + conPar.getUrl() + "]");
	try {
            if (conn != null) {
		conn.close();
                log.info("connection close OK!");
            }
	} catch (Exception e) {
            e.printStackTrace();
	}
    }
    
    
    
      
    
}
