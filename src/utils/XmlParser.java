/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import modelos.ConnParam;
import modelos.FtpParam;
import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


/**
 *
 * @author Strangexz
 */
public class XmlParser {
//    private static Logger log = Logger.getLogger(XmlParser.class);
    private static Logger log = LogManager.initLogger(XmlParser.class);
    
    /* Almacenara el archivo que contiene los datos */
    private File archivo;

    /* Almacenara el documento XML */
    private Document documento;

    /* Almacenara el nodo principal del documento */
    private Element nodoRaiz;
    
    /*Cada uno de los parametros de conexion*/
    private static String id,driver, url, ftpUrl, user, ftpUser, pass, ftpPass, tableName, lib, port , path;
    private static ConnParam conexionesBD =  null;
    private static FtpParam conexionesFTP = null;
    private static List listaConnBD = null;
    private static List listaConnFTP = null;

    public XmlParser () {
        //Aqui desarrollaremos el programa
        log.trace("BUSQUEDA DEL ARCHIVO DE CONFIGURACIÓN");
        
        /* Creamos una instancia del archivo configuracionConexiones.xml */
        archivo = new File("src/conf/configuracionConexiones.xml");
        /* Creamos el documento xml a partir del archivo File */
        SAXBuilder constructorSAX = new SAXBuilder();
        try {
            documento = (Document)constructorSAX.build(archivo);
        } catch (JDOMException e) {
            log.error("Fichero no valido");
            e.printStackTrace();
        } catch (IOException e) {
            log.error("Fichero valido");
            e.printStackTrace();
        }
        
        
        // Obtenemos el nodo raiz o principal
        nodoRaiz = documento.getRootElement();
        
        log.trace("PARCEO CONEXIONES BD");
        /* Obtenemos la lista de los nodos con la etiqueta
        * "configuracionConexiones" que se encuentran en el nodo raiz
        */
        listaConnBD = nodoRaiz.getChildren("parametrosConexionBD");
        
        /*Recorremos la lista obteniendo cada nodo, imprimiendo
        *cada elemento dentro de su aplicacion y su cateforia
        */
        for(int i = 0; i<listaConnBD.size();i++){
            /*Obtenemos el elemento (estos son los los hijos de los nodos "parametrosConexionBD"
            * o sea "MySql") de la lista
            */
            Element nodo = (Element)listaConnBD.get(i);
        
            /*Parametros de Conexion BD en MySQL*/
            id = nodo.getAttribute("id").getValue();        
        
            driver = nodo.getChild("driver").getValue();
            url = nodo.getChild("url").getValue();
            user = nodo.getChild("user").getValue();
            pass = nodo.getChild("password").getValue();
            tableName = nodo.getChild("tableName").getValue();
            lib = nodo.getChild("lib").getValue();
            
        }//fin del for
        conexionesBD = new ConnParam(url, driver, user, pass, tableName, lib);
        //Imprimimos Parametros de Conexión a la Base de Datos Local
        log.info(conexionesBD);
        
 
            log.trace("PARCEO CONEXIONES FTP");
        /* Reccorermos ahora para obtener los datos de la conexion FTP*/
        listaConnFTP = nodoRaiz.getChildren("parametrosConexionFTP");
        
        /*Recorremos la lista obteniendo cada nodo, imprimiendo
        *cada elemento dentro de su aplicacion y su cateforia
        */
        for(int i = 0; i<listaConnFTP.size();i++){
            /*Obtenemos el elemento (estos son los los hijos de los nodos "parametrosConexionFTP"
            * o sea "ServerFTP"") de la lista
            */
            Element nodo = (Element)listaConnFTP.get(i);
            
            /*Parametros de Conexion del FTP*/
            id = nodo.getAttribute("id").getValue();
            ftpUrl = nodo.getChild("url").getValue();
            ftpUser = nodo.getChild("user").getValue();
            ftpPass = nodo.getChild("password").getValue();
            port = nodo.getChild("port").getValue();
            path = nodo.getChild("path").getValue();
        }
        
        conexionesFTP = new FtpParam(ftpUrl, ftpUser, ftpPass, port, path);
         //Imprimimos Parametros de Conexión al Servidor Público Ftp de CEUTEC
        log.info(conexionesFTP);
        
        
        
    }
    
    public static ConnParam setConectionParams(){
        
        return conexionesBD;
    }
    
    public static FtpParam setFtpParams(){
        
        return conexionesFTP;
    }

    
    
    
}
