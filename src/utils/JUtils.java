/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Calendar;

/**
 *
 * @author Strangexz
 */
public class JUtils {
    
    public JUtils(){
        
    }
    
    
    public static String getNowHoraInteger(Calendar calendario) {
        StringBuilder sb = new StringBuilder();
        
        if (calendario.get(Calendar.HOUR_OF_DAY) <= 9) {
            sb.append("0").append(calendario.get(Calendar.HOUR_OF_DAY));
        } else {
            sb.append(calendario.get(Calendar.HOUR_OF_DAY));
	}

	if (calendario.get(Calendar.MINUTE) <= 9) {
		sb.append("0").append(calendario.get(Calendar.MINUTE));
	} else {
		sb.append(calendario.get(Calendar.MINUTE));
	}

	if (calendario.get(Calendar.SECOND) <= 9) {
		sb.append("0").append(calendario.get(Calendar.SECOND));
	} else {
		sb.append(calendario.get(Calendar.SECOND));
	}

	return (sb.toString());
    }
    
    public static String[] getNowFecha(Calendar calendario) {

	String[] fecha = { "0", "0", "0" };
	fecha[0] = Integer.toString(calendario.get(Calendar.DATE));
	fecha[1] = Integer.toString(calendario.get(Calendar.MONTH) + 1);
	fecha[2] = Integer.toString(calendario.get(Calendar.YEAR));

	return fecha;
    }
    
}
