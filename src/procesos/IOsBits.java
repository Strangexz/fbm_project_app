/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.util.Calendar;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import modelos.Archivo;
import org.apache.log4j.Logger;
import utils.JUtils;
import utils.LogManager;

/**
 *
 * @author Strangexz
 */
public class IOsBits {
//  BasicConfigurator.configure();
//    private static Logger log = Logger.getLogger(IOsBits.class);
    private static Logger log = LogManager.initLogger(IOsBits.class);
    
    static private File f;
    static private FileInputStream fileInput = null;
    static private BufferedInputStream bufferedInput = null;
    static private FileOutputStream fileOutput = null;
    static private BufferedOutputStream bufferedOutput = null;
    static private String datos;
    
    public static String buscarArchivo(){
        String path, ruta;
        javax.swing.JFileChooser j= new javax.swing.JFileChooser();
        j.showOpenDialog(j);
        
        if(j.getSelectedFile() == null){
            path = "";
            log.debug("no se selecciono nada");
        }else{
            path= j.getSelectedFile().getAbsolutePath();
        }
        
        f = new File(path);
        ruta = ""+f;
        log.debug("Ruta del archivo: "+ruta);
        log.debug("tamaño de la ruta: "+ruta.length());
        
        return ruta;
    }
    
    public static void crearBinario(byte[] array, int leidos, String nombreArchivo){
        try{
            
            fileOutput = new FileOutputStream(nombreArchivo+".dat");            
            bufferedOutput = new BufferedOutputStream(fileOutput);
            while (leidos > 0){
                bufferedOutput.write(array,0,leidos);
//                bufferedOutput.write(array);
                leidos=bufferedInput.read(array);
               
                
            }    
             datos = bufferedOutput.toString();
             log.debug(datos);
 
            // Cierre de los ficheros
            bufferedOutput.close();
            fileOutput.close();
        }catch(Exception ex){
            log.error(ex);
        }
    }
    public static void restaurarArchivo(byte[] array, int leidos, String nombreArchivo, String extension){
        try{
            fileOutput = new FileOutputStream(nombreArchivo+"."+extension);
            
            bufferedOutput = new BufferedOutputStream(fileOutput);
            while (leidos > 0){
                bufferedOutput.write(array,0,leidos);
                leidos=bufferedInput.read(array);
                
            }         
 
            // Cierre de los ficheros
            bufferedOutput.close();
            fileOutput.close();
        }catch(Exception ex){
            log.error(ex);
        }
    }
    
    public static void guardarArchivo(String ruta, String user){
        
        Archivo arch = new Archivo();
        File fichero = new File(ruta);
        Calendar calendario = Calendar.getInstance();
        String[] fechalog = JUtils.getNowFecha(calendario);
        FileInputStream data = null;
        
        String nombreArchivo, extension;
        int primero, ultimo;
        long tamaño = 0;
//        Blob data = null;
        
        primero = ruta.lastIndexOf("\\");       
        ultimo = ruta.lastIndexOf(".");
        if(ultimo != 0){
            extension = ruta.substring(ultimo, ruta.length());
        }else{
            extension = "";
        }
        nombreArchivo = ruta.substring(++primero, ultimo);
        
        
        log.debug("\nNombre del archivo: "+nombreArchivo+",\nExtensión: "+extension);
        
        try{
            data = new FileInputStream(fichero); 
            tamaño = fichero.length();
            
        }catch(IOException ex){
            log.error(ex);
        }
        
        arch.setNombre(nombreArchivo);
        arch.setExtension(extension);
        arch.setRuta(ruta);
        arch.setData(data);
        arch.setTamaño(String.valueOf(tamaño));
        arch.setHoraCreacion(JUtils.getNowHoraInteger(calendario));
        arch.setDiaCreacion(fechalog[0]);
        arch.setMesCreacion(fechalog[1]);
        arch.setAñoCreacion(fechalog[2]);
        arch.setUsuarioCreador(user);
        
        log.debug(arch);
        
        AdminSystem.agregarArchivo(arch);
        
    }
}
