/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos;

import utils.XmlParser;
import connections.ConnectionManager;
import connections.FtpManager;
import formularios.FrmPrincipal;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import javax.swing.JOptionPane;
import modelos.Archivo;
import org.apache.log4j.Logger;
import utils.JUtils;
import utils.LogManager;

/**
 *
 * @author Strangexz
 */
public final class AdminSystem {
    static String userLog;
//    private static Logger log =  Logger.getLogger(AdminSystem.class);
    private static Logger log = LogManager.initLogger(AdminSystem.class);
    private static FtpManager ftpCon;
    private static int idLog, respFtp = 0;
    
    private static PreparedStatement validarUsuario = null;
    private static PreparedStatement cargarArchivos = null;
    private static PreparedStatement listaUsuarios = null;
    private static PreparedStatement DatosUsuario = null;
    private static PreparedStatement datosArchivos = null;
    private static PreparedStatement agregarNuevoUsuario = null;
    private static PreparedStatement validarUsuarioExistente = null;
    private static PreparedStatement cambiarContraseñaUsuario = null;
    private static PreparedStatement agregarNuevoArchivo = null;
    private static PreparedStatement archivosPorUsuario = null;
    private static PreparedStatement cargarBitacora = null;
    private static PreparedStatement updateAvatar = null;
    private static PreparedStatement borrarArchivo = null;
    private static PreparedStatement updateXml = null;
    private static PreparedStatement registroAccionBitacora = null;
    private static PreparedStatement detalleRegistroUsuarioBitacora = null;
    private static PreparedStatement obtenerIdLog = null;
    private static PreparedStatement listaAvatares = null;
    private static PreparedStatement detalleLogueos = null;
    private static PreparedStatement validarIdLog = null;
    private static PreparedStatement obtenerArchivo = null;
    
    ConnectionManager dbMySQL = new ConnectionManager(XmlParser.setConectionParams());
    
    public AdminSystem(){
       iniciarDeclaraciones();
       
    }
    
    
    
    protected void iniciarDeclaraciones(){
        boolean echo = true;
        StringBuilder sqlQuery = new StringBuilder("");
        
        log.debug("INICIO DE LAS DECLARACIONES...");
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM USUARIOS WHERE USERNAME = ? AND CONTRASEÑA = ?".trim());        
        validarUsuario = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM USUARIOS".trim());        
        listaUsuarios = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM ARCHIVOS".trim());        
        cargarArchivos = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM USUARIOS WHERE USERNAME = ?".trim());        
        DatosUsuario = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("INSERT INTO USUARIOS (USERNAME, CONTRASEÑA, HORA_CREACION, DIA_CREACION, MES_CREACION,AÑO_CREACION, NOMBRE, ROL, AVATAR)".trim())
                .append( "VALUE(?, ?, ?, ?, ?, ?, ?, ?,?)".trim());        
        agregarNuevoUsuario = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT USERNAME FROM USUARIOS WHERE USERNAME = ?".trim());        
        validarUsuarioExistente = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("UPDATE USUARIOS SET CONTRASEÑA = ?,  HORA_CAMBIO = ?, DIA_CAMBIO = ?, MES_CAMBIO = ?, AÑO_CAMBIO = ?, USUARIO_CAMBIO = ?".trim())
                .append( " WHERE USERNAME = ? AND CONTRASEÑA = ?".trim());        
        cambiarContraseñaUsuario = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("INSERT INTO ARCHIVOS (NOMBRE, EXTENSION, TAMAÑO, RUTA, DATA, HORA_CREACION, DIA_CREACION, MES_CREACION, AÑO_CREACION, USUARIO_CREADOR, XML)".trim())
                .append( "VALUE(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)".trim());        
        agregarNuevoArchivo = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM ARCHIVOS A, EXTENSIONES E WHERE A.EXTENSION = E.EXTENSION AND A.NOMBRE = ?".trim())
                .append(" AND A.EXTENSION = ?".trim());                       
        datosArchivos = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT COUNT(*) CANT FROM ARCHIVOS WHERE USUARIO_CREADOR = ?".trim());                       
        archivosPorUsuario = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM BITACORA".trim());                       
        cargarBitacora = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("UPDATE USUARIOS SET AVATAR = ?, HORA_CAMBIO = ?, DIA_CAMBIO = ?, MES_CAMBIO = ?, AÑO_CAMBIO = ?, USUARIO_CAMBIO = ?".trim())
                .append(" WHERE USERNAME = ? AND CONTRASEÑA = ?");
        updateAvatar = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
         sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("DELETE FROM ARCHIVOS WHERE NOMBRE = ? AND EXTENSION = ?".trim());                
        borrarArchivo = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("UPDATE INTO ARCHIVOS SET XML = ? WHERE NOMBRE = ? AND EXTENSION = ?".trim());                
        updateXml = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("INSERT INTO BITACORA (USERNAME, ACCION, DESCRIPCION, ID_LOG, HORA, DIA, MES, AÑO)".trim())
                .append("VALUE(?, ?, ?, ?, ?, ?,?,?)");
        registroAccionBitacora = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT ID_LOG FROM BITACORA WHERE ACCION = 'LOGIN_FBM' AND USERNAME = ? AND AÑO = ? AND MES = ?".trim())
                .append(" AND DIA = ? AND HORA = ?".trim());                       
        obtenerIdLog = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM BITACORA WHERE ID_LOG = ?".trim());                       
        detalleRegistroUsuarioBitacora = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT AVATAR FROM USUARIOS WHERE USERNAME = ?".trim());                       
        listaAvatares = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM BITACORA WHERE USERNAME = ? AND ACCION = 'LOGIN_FBM' ".trim());                       
        detalleLogueos = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM BITACORA WHERE ID_LOG = ?".trim());                       
        validarIdLog = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
        sqlQuery.delete(0, sqlQuery.length());
        sqlQuery.append("SELECT * FROM ARCHIVOS WHERE NOMBRE = ? AND EXTENSION = ?".trim());                       
        obtenerArchivo = dbMySQL.createPreparedStatement(sqlQuery.toString());
        if(echo)log.debug(sqlQuery.toString());
        
    }
    //En el siguiente método se valida la existencia del usuario para logearse al sistema
    public static int validacionDeUsuarios(String username, String pass){
        log.debug("Validando usuario en la BD...");
        int existe = 0;
        ResultSet rs = null;
        try{
            validarUsuario.clearParameters();
             validarUsuario.setString(1, username);
             validarUsuario.setString(2, pass);
             rs = validarUsuario.executeQuery();
             
             if(rs.next()){
                 log.info("VALIDACIÓN CORRECTA, USUARIO EXISTENTE");
                 String rol = rs.getString("ROL");
                 
                 switch (rol) {
                    case "admin":
                        existe = 1;
                        break;
                    case "normal":
                        existe = 2;
                        break;
                    case "limitado":
                        existe = 3;
                        break;
                }
             }else{
                 existe = 0;
                 log.info("VALIDACIÓN CORRECTA, EL USUARIO NO EXISTE O CONTRASEÑA INCORRECTA");
                 JOptionPane.showMessageDialog(null, "USUARIO NO EXISTE O CONTRASEÑA INCORRECTA", "LOGIN", JOptionPane.WARNING_MESSAGE);
             }
        }catch(SQLException e){
            log.error(e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
	}
        
        return existe;
    }
    
    public static void entrandoAlSistema(String username){
        userLog = username;
        idLog = generadorIdLog();
        Main.frmLog.setVisible(false);
        Main.frmPrime = new FrmPrincipal();
//        Manager principal = new Manager();
        Main.frmPrime.setVisible(true);
//        principal.setVisible(true);  
        registroBitacora("LOGIN_FBM", "El usuario ["+userLog+"] se conecto al sistema");
    }
    
    public static String usuarioLogeado(){
        int param = 1;           
        log.debug("Usuario Logueado: "+userLog);
        
        return userLog;
    }
    
    public static ResultSet cargandoUsuarios(){        
        ResultSet rs = null;
        try{
            rs = listaUsuarios.executeQuery();
            
        }catch(SQLException e){
            log.error(e);        
        }
        
        return rs;
        
    }
    
    public static String[] cargandoDatosUsuarios(String username){
        int i = 0;
        ResultSet rs = null;
        String[] dataUser =  new String[7];
        String validator;
        try{
            DatosUsuario.clearParameters();
            DatosUsuario.setString(1, username);             
            rs = DatosUsuario.executeQuery();
            log.debug("username: "+username+" Result: "+rs.next());
            dataUser[i++] = rs.getString("NOMBRE");
            dataUser[i++] = rs.getString("ROL");
            if(rs.getString("AVATAR") != null){
                dataUser[i++] = "SI";
            }else{
                dataUser[i++] = "";
            }
            dataUser[i++] = rs.getString("AÑO_CREACION")+"/"+rs.getString("MES_CREACION")+"/"+rs.getString("DIA_CREACION");
            validator = rs.getString("AÑO_CAMBIO");
            log.debug("validator: "+validator);
            if(validator != null){
                dataUser[i++] = rs.getString("AÑO_CAMBIO")+"/"+rs.getString("MES_CAMBIO")+"/"+rs.getString("DIA_CAMBIO");                
                dataUser[i++] = rs.getString("USUARIO_CAMBIO");
            }else{
                dataUser[i++] = "";
            }
            
            dataUser[i++] = contarArchivosUsuario(username);
            
            
            
        }catch(SQLException e){
            log.error("ERROR AL CARGAR DATOS DEL USUARIO"+e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
	}
//        log.debug("username: "+username+" Result: "+rs);
        return dataUser;
        
    }
    
    public static ResultSet cargarArchivos(){
        ResultSet rs = null;
        
        try{
            log.info("CARGANDO LAS LISTAS DE ARCHIVOS");
            cargarArchivos.clearParameters();
            rs = cargarArchivos.executeQuery();
            
        }catch(SQLException e){
            log.error("ERROR AL CARGAR ARCHIVOS"+e);        
        }
        
        return rs;
    }
    
    public static String cargarUltimoLoginUsuario(String username){
        String login = null;
        ResultSet rs = null;         
        try{
            cargarBitacora.clearParameters();
            cargarBitacora.setString(1, username);
            rs = cargarBitacora.executeQuery();
            login = rs.getString("CANT");
            
        }catch(SQLException e){
            log.error("ERROR AL CONTAR LOS ARCHIVOS PRO USUARIO: "+e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
	}
        
        return login;
    }
	
	public static String contarArchivosUsuario(String username){
         ResultSet rs = null;
         String cantidad = null;
        try{
            archivosPorUsuario.clearParameters();
            archivosPorUsuario.setString(1, username);
            rs = archivosPorUsuario.executeQuery();
            if(rs.next())
                cantidad = rs.getString("CANT");
            
        }catch(SQLException e){
            log.error("ERROR AL CONTAR LOS ARCHIVOS POR USUARIO:"+e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
	}
        
        return cantidad;
    }
        
    public static void agregarUsuario(String username, String contraseña, String rol, String nombreReal, FileInputStream avatar, long tamaño){
        int param = 1; 
        
        try{
            log.debug("agregando...");
            
            agregarNuevoUsuario.setString(param++, username);
            agregarNuevoUsuario.setString(param++, contraseña);            
            Calendar calendario = Calendar.getInstance();
            agregarNuevoUsuario.setString(param++, JUtils.getNowHoraInteger(calendario));
            String[] fechaLog=  new SimpleDateFormat("dd/MM/yyyy").format(calendario.getTime()).split("/");//fecha.split("/");
            agregarNuevoUsuario.setString(param++, fechaLog[0]);
            agregarNuevoUsuario.setString(param++, fechaLog[1]);
            agregarNuevoUsuario.setString(param++, fechaLog[2]);
            agregarNuevoUsuario.setString(param++, nombreReal);
            agregarNuevoUsuario.setString(param++, rol); 
            agregarNuevoUsuario.setBlob(param++, avatar, tamaño);
//            agregarNuevoUsuario.setString(param++, userLog);            
            agregarNuevoUsuario.executeUpdate();
            
            log.info("USUARIO AGREGADO EXITOSAMENTE");
            JOptionPane.showMessageDialog(null, "AGREGADO NUEVO USUARIO EXITOSAMENTE", "INFO", JOptionPane.INFORMATION_MESSAGE);
            
            
            
            
        }catch(SQLException ex){
            log.error(ex);
        }
    }
    
    public static boolean usuarioExistente(String username){
        boolean existe=false;
        
        ResultSet rsExiste;
        try{
            validarUsuarioExistente.clearParameters();
             validarUsuarioExistente.setString(1, username);             
             rsExiste = validarUsuarioExistente.executeQuery();
             
             existe = rsExiste.next();
             
        }catch(SQLException ex){
            log.error(ex);
        }
        
        return existe;
    }
    
    public static void cambiarContraseña(String nuevoPass, String actualPass, String username){
        ResultSet rsCambio;
        
        
        try{
             validarUsuario.clearParameters();
             validarUsuario.setString(1, username);
             validarUsuario.setString(2, actualPass);
             
             rsCambio = validarUsuario.executeQuery();
             int param = 1;
             if(rsCambio.next()){
                cambiarContraseñaUsuario.clearParameters();
                cambiarContraseñaUsuario.setString(param++, nuevoPass);                
                Calendar calendario = Calendar.getInstance();
                cambiarContraseñaUsuario.setString(param++,  JUtils.getNowHoraInteger(calendario));                 
                String[] fechaLog=  new SimpleDateFormat("dd/MM/yyyy").format(calendario.getTime()).split("/");//fecha.split("/");
                cambiarContraseñaUsuario.setString(param++, fechaLog[0]);
                cambiarContraseñaUsuario.setString(param++, fechaLog[1]);
                cambiarContraseñaUsuario.setString(param++, fechaLog[2]);
                cambiarContraseñaUsuario.setString(param++, userLog);
                cambiarContraseñaUsuario.setString(param++, username);  
                cambiarContraseñaUsuario.setString(param++, actualPass); 
                cambiarContraseñaUsuario.executeUpdate();
                log.info("SE CAMBIO EXITOSAMENTE LA CONTRASEÑA DEL USUARIO ["+username+"]");
                JOptionPane.showMessageDialog(null, "SE CAMBIO EXITOSAMENTE LA CONTRASEÑA DEL USUARIO ["+username+"]", "CAMBIO EXITOSO", JOptionPane.INFORMATION_MESSAGE);
                 registroBitacora("CHANGE", "El usuario ["+userLog+"] cambio la contraseña del usuario ["+username+"]");
             }else{
                log.info("NO SE CAMBIO LA CONTRASEÑA DEL USUARIO ["+username+"], LA CONTRASEÑA ES INCORRECTA");
                JOptionPane.showMessageDialog(null, "NO SE CAMBIO LA CONTRASEÑA DEL USUARIO ["+username+"], LA CONTRASEÑA ES INCORRECTA", "AVISO", JOptionPane.WARNING_MESSAGE);              
             }
        }catch(SQLException ex){
            log.error(ex);
        }
    }
    
    public static void eliminarUsuario(){
       
        
    }
    
    public static void agregarArchivo(Archivo fichero){
        FileInputStream fis;
        int param = 1;
        
        try{
           
            agregarNuevoArchivo.clearParameters();
            agregarNuevoArchivo.setString(param++, fichero.getNombre());
            agregarNuevoArchivo.setString(param++,fichero.getExtension());
            agregarNuevoArchivo.setString(param++,fichero.getTamaño() );
            agregarNuevoArchivo.setString(param++,fichero.getRuta());           
            agregarNuevoArchivo.setBlob(param++,fichero.getData());           
            agregarNuevoArchivo.setString(param++,fichero.getHoraCreacion());
            agregarNuevoArchivo.setString(param++,fichero.getDiaCreacion());
            agregarNuevoArchivo.setString(param++,fichero.getMesCreacion());
            agregarNuevoArchivo.setString(param++,fichero.getAñoCreacion());
            agregarNuevoArchivo.setString(param++,fichero.getUsuarioCreador());
            fis = generarXml(fichero);
            agregarNuevoArchivo.setBlob(param++,fis);
            agregarNuevoArchivo.executeUpdate();
            
            registroBitacora("ADD", "Se guardo el archivo ["+fichero.getNombre()+fichero.getExtension()+"]");
            insertBitacora();
            
            log.info("ARCHIVO AGREGADO EXITOSAMENTE");
            JOptionPane.showMessageDialog(null, "ARCHIVO NUEVO AGREGADO EXITOSAMENTE", "INFO", JOptionPane.INFORMATION_MESSAGE);
            
        }catch(SQLException ex){
             log.error("ERROR AL AGREGAR ARCHIVO ["+ex+"]");
            JOptionPane.showMessageDialog(null, "ERROR AL AGREGAR ARCHIVO", "ERROR", JOptionPane.ERROR_MESSAGE);
            
        }
        
    }
    
    public static void eliminarArchivo(String filename, String ext){
        try{
            borrarArchivo.clearParameters();
            borrarArchivo.setString(1, filename);
            borrarArchivo.setString(2, ext);
            borrarArchivo.executeUpdate();
            log.info("EL ARCHIVO ["+filename+ext+"] HA SIDO BORRADO");
        }catch(SQLException ex){
            log.error("NO SE PUDO BORRAR EL ARCHIVO["+filename+ext+"]:"+ex);
        }
        registroBitacora("DELETE", "Eliminado el archivo["+filename+ext+"]");
        insertBitacora();
    }
    
    public static FileInputStream generarXml(Archivo fichero){
        FileInputStream fis = null;
        
        File fileXml = new File("ficherosXml/"+fichero.getNombre()+".xml");
        
        if(fileXml.exists()){
            
            
        }else{
             log.info("EL ARCHIVO "+fichero.getNombre()+".xml NO EXISTE SE PROCEDERA A CREARSE");
            try{
                PrintWriter out= new PrintWriter(fileXml);
                out.println("<?xml version='1.0' encoding='utf-8'?>");
                out.println("<root>");
                out.println("   <archivo nombre=\""+fichero.getNombre()+"\">");
                out.println("       <extension>"+fichero.getExtension()+"</extension>");
                out.println("       <ruta>"+fichero.getRuta()+"</ruta>");
                out.println("       <tamaño>"+fichero.getTamaño()+"</tamaño>");
                out.println("       <data>"+fichero.getData()+"</data>");
                out.println("       <creador>"+fichero.getUsuarioCreador()+"</creador>");
                out.println("       <fechacreacion>"+fichero.getAñoCreacion()+"-"+fichero.getMesCreacion()+"-"+fichero.getDiaCreacion()+"</fechacreacion>");
                out.println("       <horacreacion>"+fichero.getHoraCreacion()+"</horacreacion>");
                out.println("   </archivo>");
                out.println("</root>");

                out.close();
                
                fis = new FileInputStream(fileXml);
                
            }
            catch (FileNotFoundException e)
            {
                log.error("ERROR AL GENEREAR EL ARCHIVO XML: "+e);
                JOptionPane.showMessageDialog(null, "ERROR AL GENERAR EL ARCHIVO XML", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        return fis;
        
    }
    
    public static void generarXml(String filename, String extension){
        
        Archivo fichero = new Archivo();
        
        ResultSet rs = null;        
        String xml;
        int i = 0;
        log.info("CARGANDO DATOS...");
        try{
            datosArchivos.clearParameters();
            datosArchivos.setString(1, filename);             
            datosArchivos.setString(2, extension);
            rs = datosArchivos.executeQuery();
            log.debug("filename: "+filename+" Result: "+rs.next());
            fichero.setNombre(rs.getString("NOMBRE"));
            fichero.setRuta(rs.getString("RUTA"));
            fichero.setTamaño(rs.getString("TAMAÑO"));
            fichero.setData(rs.getObject(("DATA"), FileInputStream.class));
            fichero.setHoraCreacion(rs.getString("HORA_CREACION"));
            fichero.setAñoCreacion(rs.getString("AÑO_CREACION"));
            fichero.setMesCreacion(rs.getString("MES_CREACION"));
            fichero.setDiaCreacion(rs.getString("DIA_CREACION"));
            fichero.setExtension(rs.getString("EXTENSION"));
            fichero.setUsuarioCreador(rs.getString("USUARIO_CREADOR"));                      
            
        }catch(SQLException e){
            log.error(e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
	}
        
        File fileXml = new File("ficherosXml/"+fichero.getNombre()+".xml");
        FileInputStream fisXml = null;
        try{
           fisXml = new FileInputStream(fileXml); 
        }catch(FileNotFoundException e){
            log.error(e);
        }
        
        
        if(fileXml.exists()){
            log.info("YA EXISTE!");
            updateArchivoXml(filename, extension, fisXml);
            
        }else{
             log.info("CREANDO EL ARCHIVO "+fichero.getNombre()+".xml");
            try{
                PrintWriter out= new PrintWriter(fileXml);
                out.println("<?xml version='1.0' encoding='utf-8'?>");
                out.println("<root>");
                out.println("   <archivo nombre=\""+fichero.getNombre()+"\">");
                out.println("       <extension>"+fichero.getExtension()+"</extension>");
                out.println("       <ruta><![CDATA["+fichero.getRuta()+"]]></ruta>");
                out.println("       <tamaño>"+fichero.getTamaño()+"</tamaño>");
                out.println("       <data>"+fichero.getData()+"</data>");
                out.println("       <creador>"+fichero.getUsuarioCreador()+"</creador>");
                out.println("       <fechacreacion>"+fichero.getAñoCreacion()+"-"+fichero.getMesCreacion()+"-"+fichero.getDiaCreacion()+"</fechacreacion>");
                out.println("       <horacreacion>"+fichero.getHoraCreacion()+"</horacreacion>");
                out.println("   </archivo>");
                out.println("</root>");

                out.close();
                
                log.info("ARCHIVO"+fichero.getNombre()+".xml CREADO");
                JOptionPane.showMessageDialog(null, "EL ARCHIVO XML HA SIDO CREADO", "INFO", JOptionPane.INFORMATION_MESSAGE);
                updateArchivoXml(filename, extension, fisXml);
            }
            catch (FileNotFoundException e)
            {
                log.error("ERROR AL GENEREAR EL ARCHIVO XML: "+e);
                JOptionPane.showMessageDialog(null, "ERROR AL GENERAR EL ARCHIVO XML", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public static String[] cargandoDataArchivos(String filename, String extension){
        String dataFile[] = new String[15];
        
        ResultSet rs = null;        
        String validator;
        int i = 0;
        try{
            datosArchivos.clearParameters();
            datosArchivos.setString(1, filename);             
            datosArchivos.setString(2, extension); 
            rs = datosArchivos.executeQuery();
            log.debug("filename: "+filename+" Result: "+rs.next());
            dataFile[i++] = rs.getString("RUTA");
            dataFile[i++] = rs.getString("TAMAÑO");
            dataFile[i++] = rs.getString("AÑO_CREACION")+"/"+rs.getString("MES_CREACION")+"/"+rs.getString("DIA_CREACION");
            dataFile[i++] = rs.getString("EXTENSION");
            dataFile[i++] = rs.getString("USUARIO_CREADOR");
            dataFile[i++] = rs.getString("TIPO");
            dataFile[i++] = rs.getString("DESCRIPCION");
            dataFile[i++] = rs.getString("HORA_CREACION");
                       
            
        }catch(SQLException e){
            log.error(e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
	}
//        log.debug("filename: "+filename+" Result: "+rs);
        return dataFile;
    }
    
    public static String cargarXml(String nombreArchivo, String extension){
        String xml = null;
        FileInputStream xmlGen = null;        
        File fileXml = new File("ficherosXml/"+nombreArchivo+".xml");
        
        if(fileXml.exists()){
            log.info("EL ARCHIVO ["+nombreArchivo+".xml] YA EXISTE, SE PROCEDERÁ A CARGARSE");
            try{
                FileReader lectorArchivo = new FileReader(fileXml);
                BufferedReader br = new BufferedReader(lectorArchivo);
                String l="";
                String aux="";
                
                while(true){
                    aux=br.readLine();
                    if(aux!=null)
                        l=l+aux+"\n";
                    else
                        break;  
                }                
                br.close();
                lectorArchivo.close();  
                
                xml = l;
                
                xmlGen = new FileInputStream(fileXml);
                log.info("EL ARCHIVO["+nombreArchivo+".xml] FUE CARGADO EXITOSAMENTE!!!");
                
            }catch(IOException e){
                log.error("NO SE PUDO CARGAR EL ARCHIVO"+nombreArchivo+".xml: "+e.getMessage());
            }
        }else{
            log.info("EL ARCHIVO "+nombreArchivo+".xml NO EXISTE SE PROCEDERA A CREARSE");
            JOptionPane.showMessageDialog(null, "EL ARCHIVO XML NO EXISTE SE PROCEDERÁ A CREARSE", "INFO", JOptionPane.INFORMATION_MESSAGE);
            generarXml(nombreArchivo, extension);
            registroBitacora("SHOW","Se mostro el archivo XML["+nombreArchivo+"]");
        }
            
         return xml;
    }
    
    public static void insertBitacora(){
        int i = 0;
        ResultSet rs = null;
        StringBuilder query = new StringBuilder("");
             
        query.append("INSERT INTO BITACORA ")
                .append("(USERNAME, ACCION, DESTINO, ID_LOG, HORA, DIA, MES, AÑO)")
                .append("\nVALUE\n");
        
        try{
            cargarBitacora.clearParameters();
            rs = cargarBitacora.executeQuery();
            
            while(rs.next()){
                query.append("('")
                    .append(rs.getString("USERNAME"))
                    .append("','")
                    .append(rs.getString("ACCION"))
                    .append("','")
                    .append(rs.getString("DESTINO"))
                    .append("','")
                    .append(rs.getString("ID_LOG"))
                    .append("','")
                    .append(rs.getString("HORA"))
                    .append("','")
                    .append(rs.getString("DIA"))
                    .append("','")
                    .append(rs.getString("MES"))
                    .append("','")
                    .append(rs.getString("AÑO"))
                    .append("),\n");
            }
        }catch(SQLException e){
            log.error(e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
	}
        File insertSql = new File("sql/insertBitacora.sql");
             log.info("GENERADO INSERT: \n"+query);
        
        try{
            PrintWriter out= new PrintWriter(insertSql);
            out.println(query);
            out.close();
        }catch (FileNotFoundException e){
            log.error("NO SE GENERÓ EL INSERT :"+e);
        }
    }
    
    public static void updateArchivoXml(String filename, String extension, FileInputStream xml){
        try{
        
            updateXml.clearParameters();
            updateXml.setBlob(1, xml);
            updateXml.setString(2, filename);
            updateXml.setString(3, extension);
            updateXml.executeUpdate();
            log.info("EL CAMPO XML DEL ARCHIVO["+filename+"."+extension+"] HA SIDO ACTUALIZADO");
        }catch(SQLException ex){
            log.error("NO SE ACTUALIZO EL XML: "+ex);
        }
    }
    
    public static boolean conexionFtp(){
        boolean conectado;
        log.trace("creando el objeto de conexion");
        ftpCon = new FtpManager(XmlParser.setFtpParams());
        registroBitacora("LOGIN_FTP", "El usuario ["+userLog+"] se conecto al servidor FTP");
        
        conectado = FtpManager.conectado;
        log.debug("conectado: "+conectado);
        return conectado;
    }
    
    public static void desconexionFtp(){
        FtpManager.closeConectionFTP();
        registroBitacora("LOGOUT_FTP", "El usuario ["+userLog+"] se desconecto al servidor FTP");
    }
    
    public static void registroBitacora(String accion, String descripcion){
        int param = 1;
        
        try{
            registroAccionBitacora.clearParameters();
            registroAccionBitacora.setString(param++, userLog);
            registroAccionBitacora.setString(param++, accion);
            registroAccionBitacora.setString(param++, descripcion);
            registroAccionBitacora.setInt(param++, idLog);
            Calendar calendario = Calendar.getInstance();
            registroAccionBitacora.setString(param++, JUtils.getNowHoraInteger(calendario));
            String[] fechaLog=  new SimpleDateFormat("dd/MM/yyyy").format(calendario.getTime()).split("/");//fecha.split("/");
            registroAccionBitacora.setString(param++, fechaLog[0]);
            registroAccionBitacora.setString(param++, fechaLog[1]);
            registroAccionBitacora.setString(param++, fechaLog[2]);
            registroAccionBitacora.executeUpdate();
            log.info("ACCIÓN ["+accion+"] DE ["+userLog+"] GUARDADO EN BITACORA");
        }catch(SQLException ex){
            log.error(ex);
        }
        
    }
    
    public static int generadorIdLog(){
        int generado = 0;
        ResultSet rs = null;
                      
        try{
            do{
                Random random = new Random(System.currentTimeMillis());
                generado = random.nextInt();
                validarIdLog.clearParameters();
                validarIdLog.setInt(1, generado);
                rs = validarIdLog.executeQuery();  
                
                if(rs.next()){
                    log.info("EL ID_LOG["+generado+"] YA EXISTE, SE GENERARÁ UNO NUEVO");
                }else{
                    log.info("EL ID_LOG["+generado+"] GENERADO");
                }
                
            }while(rs.next());
        }catch(SQLException e){
            log.error(e);
        }
        
        return generado;
    }
    
    public static Blob cargandoAvataresUsuarios(String user){
        ResultSet rs;
        Blob avatar=null;
        int param = 1;
        
        try{
            listaAvatares.clearParameters();
            listaAvatares.setString(param++, user);            
            rs = listaAvatares.executeQuery();
            if(rs.next()){
                avatar = rs.getBlob("AVATAR");
            }
        }catch(SQLException ex){
            log.error(ex);
        }
        
        return avatar;
    }
    
    public static ResultSet cargarDetalleLogueos(String user){
        ResultSet rs = null;
        
        try{
            detalleLogueos.clearParameters();
            detalleLogueos.setString(1, user);
            rs = detalleLogueos.executeQuery();
            
        }catch(SQLException ex){
            log.error(ex);
        }
        
        return rs;      
        
    }
    
    public static int obtenerIdLogUser(String user, String fecha[]){
        ResultSet rs = null;
        int idL = 0, i = 0, param = 1;
        
        try{
            obtenerIdLog.clearParameters();
            obtenerIdLog.setString(param++, user);
            obtenerIdLog.setString(param++, fecha[i++]);
            obtenerIdLog.setString(param++, fecha[i++]);
            obtenerIdLog.setString(param++, fecha[i++]);
            obtenerIdLog.setString(param++, fecha[i++]);
            rs = obtenerIdLog.executeQuery();
            
            if(rs.next()){
                idL = rs.getInt("ID_LOG");
                log.info("ID_LOG["+idL+"] ENCONTRADO");
            }else{
                log.info("NO SE ENCONTRO ID_LOG");
            }
            
        }catch(SQLException ex){
            log.error(ex);
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}				
        }
        
        return idL;
        
    }
    
    public static ResultSet cargarDetallesBitacora(int idL){
        ResultSet rs = null;
        
        try{
            detalleRegistroUsuarioBitacora.clearParameters();
            detalleRegistroUsuarioBitacora.setInt(1, idL);
            rs = detalleRegistroUsuarioBitacora.executeQuery();
        }catch(SQLException ex){
            log.error(ex);
        }
        
        return rs;
        
    }
    
    public static void cambiarAvatar(String user, String pass, FileInputStream avatar, long tam){
        ResultSet rs;
        int param = 1;
        
        try{
            validarUsuario.clearParameters();
             validarUsuario.setString(1, user);
             validarUsuario.setString(2, pass);
             
             rs = validarUsuario.executeQuery();
             
             if(rs.next()){
                updateAvatar.clearParameters();
                updateAvatar.setBlob(param++, avatar, tam);               
                Calendar calendario = Calendar.getInstance();
                updateAvatar.setString(param++, JUtils.getNowHoraInteger(calendario));
                String[] fechaLog=  new SimpleDateFormat("dd/MM/yyyy").format(calendario.getTime()).split("/");//fecha.split("/");
                updateAvatar.setString(param++, fechaLog[0]);
                updateAvatar.setString(param++, fechaLog[1]);
                updateAvatar.setString(param++, fechaLog[2]);
                updateAvatar.setString(param++, userLog);
                updateAvatar.setString(param++, user);
                updateAvatar.setString(param++, pass);
                updateAvatar.executeUpdate();  
                log.info("SE CAMBIO EXITOSAMENTE EL AVATAR DEL USUARIO ["+user+"]");
                JOptionPane.showMessageDialog(null, "SE CAMBIO EXITOSAMENTE EL AVATAR DEL USUARIO ["+user+"]", "CAMBIO EXITOSO", JOptionPane.INFORMATION_MESSAGE);
                 registroBitacora("CHANGE", "Se cambio el avatar del usuario: "+user);
             }else{
                log.info("NO SE CAMBIO EL AVATAR DEL USUARIO ["+user+"], LA CONTRASEÑA ES INCORRECTA");
                JOptionPane.showMessageDialog(null, "NO SE CAMBIO EL AVATAR DEL USUARIO ["+user+"], LA CONTRASEÑA ES INCORRECTA", "AVISO", JOptionPane.WARNING_MESSAGE);              
             }
            
            
        }catch(SQLException e){
            log.error(e);
        }
    }
    
    public static Archivo cargandoArchivo(String nombreArchivo, String extension){
        Archivo fichero = new Archivo();
        ResultSet rs;
        
        try{
            obtenerArchivo.clearParameters();
            obtenerArchivo.setString(1, nombreArchivo);
            obtenerArchivo.setString(2, extension);
            rs = obtenerArchivo.executeQuery();
            
            if(rs.next()){
                fichero.setNombre(rs.getString("NOMBRE"));
                fichero.setExtension(rs.getString("EXTENSION"));
                fichero.setRuta(rs.getString("RUTA"));
                fichero.setTamaño(rs.getString("TAMAÑO"));
                fichero.setXml(rs.getBlob("XML"));
//                fichero.setData(rs.getObject("DATA", FileInputStream.class));
            }else{
                log.info("EL ARCHIVO ["+nombreArchivo+extension+"] NO EXISTE EN LA BASE DE DATOS");
                JOptionPane.showMessageDialog(null, "EL ARCHIVO ["+nombreArchivo+extension+"] NO EXISTE EN LA BASE DE DATOS",
                        "INFO", JOptionPane.INFORMATION_MESSAGE);
            }
        }catch(SQLException e){
            log.error(e);
        }
        
        return fichero;
        
    }
    
    public static void subirArchivoFtp(Archivo fichero, boolean bin, boolean xml){
        
        if(bin){
           try{
               File fileBin = new File(fichero.getRuta());
               if(fileBin.exists()){
                  FtpManager.uploadFileFTP(fileBin, fichero.getNombre()+fichero.getExtension(), "/30911324"); 
               }else{
                   log.info("NO EXISTE BINARIO");
               } 
           }catch(Exception e){
               log.error(e);
           }
            
        }else if(xml){
            try{
                File fileXml = new File("ficherosXml/"+fichero.getNombre()+".xml");
                
                if(fileXml.exists()){
                    log.info("EL ARCHIVO XML EXISTE");
                    FtpManager.uploadFileFTP(fileXml,fichero.getNombre()+".xml", "/30911324");
                }else{
                    log.info("NO EXISTE XML");
                }
            }catch(Exception e){
                log.error(e);
            }
        }else{
            log.info("NO SE HA SELECCIONADO EL FORMATO DEL ARCHIVO A SUBIR");
            JOptionPane.showMessageDialog(null, "NO SE HA SELECCIONADO EL FORMATO DEL ARCHIVO A SUBIR", "AVISO", JOptionPane.WARNING_MESSAGE);
        }
        
    }
   
    
    
}
