/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

/**
 *
 * @author Strangexz
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelos.Archivo;
import org.apache.log4j.Logger;
import procesos.AdminSystem;
import utils.LogManager;

public class FrmFilesTable extends javax.swing.JFrame {

    /**
     * Creates new form FrmFilesTable
     */
//    private static Logger log =  Logger.getLogger(FrmFilesTable.class);
    private static Logger log = LogManager.initLogger(FrmFilesTable.class);
    JTable tablaU, tablaA;  
    List<Archivo> listaArchivos;
    public FrmFilesTable() {
        initComponents();
        ResultSet rs;
        DefaultTableModel dtm = new DefaultTableModel();
        
        tablaA = this.jtTablaArchivos;
        tablaA.setModel(dtm);
        
        dtm.setColumnIdentifiers(new Object[]{"ID", "NOMBRE", "EXT", "DATA",
            "TAMA", "USRCR", "HORA", "DIA", "MES", "AÑO", "XML"});
        
        rs = AdminSystem.cargarArchivos();
        
        try{
            int count = 0;
            
            log.info("CARGANDO TABLA DE USUARIOS");
            
            
            this.listaArchivos = new LinkedList<>();
            while(rs.next()){
                dtm.addRow(new Object[]{rs.getString("ID"),rs.getString( "NOMBRE"),rs.getString("EXTENSION"),rs.getString("DATA"),rs.getString("TAMAÑO"),
                    rs.getString("USUARIO_CREADOR"),rs.getString("HORA_CREACION"),rs.getString("DIA_CREACION"),rs.getString("MES_CREACION"),rs.getString("AÑO_CREACION"),
                    rs.getString("XML")});
                count++;
                
                Archivo tmp = new Archivo();
                
                tmp.setNombre(rs.getString("NOMBRE").trim());
                tmp.setExtension(rs.getString("EXTENSION").trim());
                tmp.setRuta(rs.getString("RUTA").trim());
//                tmp.setData((FileInputStream) rs.getObject("DATA"));
                tmp.setTamaño(rs.getString("TAMAÑO").trim());
                tmp.setUsuarioCreador(rs.getString("USUARIO_CREADOR").trim());
                tmp.setHoraCreacion(rs.getString("HORA_CREACION").trim());
                tmp.setDiaCreacion(rs.getString("DIA_CREACION").trim());
                tmp.setMesCreacion(rs.getString("MES_CREACION").trim());
                tmp.setAñoCreacion(rs.getString("AÑO_CREACION").trim());
                if(rs.getBlob("XML") == null){
                    log.debug("El campo XML esta vacio");
                }else{
                    tmp.setXml(rs.getBlob("XML"));
                }
                
                this.listaArchivos.add(tmp);
                log.debug("Lista <<"+listaArchivos+">>");
                
            }
            
            log.info("ARCHIVOS CARGADOS ["+count+"]");
        }catch(SQLException e){
            log.error(e);        
        }finally{
            try{if(rs!=null)rs.close();}catch(Exception e){log.error(e);}            
	}
    }
    
    public static void insertSqlArchivos(List<Archivo> lista){
        log.info(lista);
        
        StringBuilder query = new StringBuilder("");
        StringBuilder value = new StringBuilder("");
             
        query.append("INSERT INTO ARCHIVOS ")
                .append("(NOMBRE, EXTENSION, RUTA,DATA, HORA_CREACION, DIA_CREACION, MES_CREACION, AÑO_CREACION, TAMAÑO, USUARIO_CREADOR, XML)")
                .append("\nVALUE\n");
       
        for(Archivo tmp : lista){
            query.append("('")
                 .append(tmp.getNombre())
                 .append("','")
                 .append(tmp.getExtension())
                 .append("','")
                 .append(tmp.getRuta())
                 .append("','")
                 .append(tmp.getData())
                 .append("','")
                 .append(tmp.getHoraCreacion())
                 .append("','")
                 .append(tmp.getDiaCreacion())
                 .append("','")
                 .append(tmp.getMesCreacion())
                 .append("','")
                 .append(tmp.getAñoCreacion())
                 .append("','")
                 .append(tmp.getTamaño())
                 .append("','")
                 .append(tmp.getUsuarioCreador())
                 .append("',")
                 .append(tmp.getXml())
                 .append("),\n");          
        }
//        query.append(value);
         File insertSql = new File("sql/insertArchivos.sql");
         log.info("GENERAO INSERT: \n"+query);
        
        try{
            PrintWriter out= new PrintWriter(insertSql);
            out.println(query);
            
            out.close();
        }catch (FileNotFoundException e){
            log.error("NO SE GENERÓ EL INSERT :"+e);
        
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jtTablaArchivos = new javax.swing.JTable();

        setTitle("Tabla de Archivos");

        jtTablaArchivos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jtTablaArchivos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmFilesTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmFilesTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmFilesTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmFilesTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmFilesTable().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtTablaArchivos;
    // End of variables declaration//GEN-END:variables
}
