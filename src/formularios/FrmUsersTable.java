/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

/**
 *
 * @author Wing Zero
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelos.Archivo;
import modelos.Usuario;
import org.apache.log4j.Logger;
import procesos.AdminSystem;
import utils.LogManager;

public class FrmUsersTable extends javax.swing.JFrame {

    /**
     * Creates new form FrmUsersTable
     */
//    private static Logger log =  Logger.getLogger(FrmUsersTable.class);
    private static Logger log = LogManager.initLogger(FrmUsersTable.class);
    JTable tablaU, tablaA;
    public FrmUsersTable() {
        initComponents();
        List<Usuario> listaUsuarios = new LinkedList<>();
        DefaultTableModel dtm = new DefaultTableModel();
        ResultSet rsU;
        tablaU = this.jtTablaUsuarios;
        tablaU.setModel(dtm);
        
        dtm.setColumnIdentifiers(new Object[]{"USER", "PASS", "HORA", "DIA",
            "MES", "AÑO", "NAME", "ROL", "HORAC","DIAC","MESC", "AÑOC", "USRC"});
        
        rsU = AdminSystem.cargandoUsuarios();
        
        try{
            int count = 0;
            
            log.info("CARGANDO TABLA DE USUARIOS");
            while(rsU.next()){
                dtm.addRow(new Object[]{rsU.getString("USERNAME"),rsU.getString( "CONTRASEÑA"),rsU.getString("HORA_CREACION"),rsU.getString("DIA_CREACION"),
                rsU.getString("MES_CREACION"),rsU.getString("AÑO_CREACION"),rsU.getString("NOMBRE"),rsU.getString("ROL"),rsU.getString("HORA_CAMBIO"),
                rsU.getString("DIA_CAMBIO"),rsU.getString("MES_CAMBIO"),rsU.getString("AÑO_CAMBIO"),rsU.getString("USUARIO_CAMBIO")});
                count++;
                
                Usuario tmp = new Usuario();
                tmp.setUser(rsU.getString("USERNAME"));
                tmp.setContraseña(rsU.getString( "CONTRASEÑA"));
                tmp.setHoraCreacion(rsU.getString("HORA_CREACION"));
                tmp.setDiaCreacion(rsU.getString("DIA_CREACION"));
                tmp.setMesCreacion(rsU.getString("MES_CREACION"));
                tmp.setAñoCreacion(rsU.getString("AÑO_CREACION"));
                tmp.setNombre(rsU.getString("NOMBRE"));
                tmp.setRol(rsU.getString("ROL"));
                if(rsU.getString("HORA_CAMBIO") != null){
                    tmp.setHoraCambio(rsU.getString("HORA_CAMBIO"));
                    tmp.setDiaCambio(rsU.getString("DIA_CAMBIO"));
                    tmp.setMesCambio(rsU.getString("MES_CAMBIO"));
                    tmp.setAñoCambio(rsU.getString("AÑO_CAMBIO"));
                    tmp.setUsuarioCambio(rsU.getString("USUARIO_CAMBIO"));
                }
               listaUsuarios.add(tmp);
            }
            log.info("USUARIOS CARGADOS ["+count+"]");
            insertaSqlUsuarios(listaUsuarios);
        }catch(SQLException e){
            log.error(e);        
        }finally{            
            try{if(rsU!=null)rsU.close();}catch(Exception e){log.error(e);}
	}
        
    }
    
    public static void insertaSqlUsuarios(List<Usuario> lista){
        log.info(lista);
        
        StringBuilder query = new StringBuilder("");
             
        query.append("INSERT INTO ARCHIVOS ")
                .append("(USERNAME, CONTRASEÑA, HORA_CREACION,DIA_CREACION, MES_CREACION, AÑO_CREACION, NOMBRE, ROL, HORA_CAMBIO,DIA_CAMBIO, MES_CAMBIO, AÑO_CAMBIO, USUARIO_CAMBIO)")
                .append("\nVALUE\n");
       
        for(Usuario tmp : lista){
            query.append("('")
                 .append(tmp.getUser())
                 .append("','")
                 .append(tmp.getContraseña())
                 .append("','")
                 .append(tmp.getHoraCreacion())
                 .append("','")
                 .append(tmp.getDiaCreacion())
                 .append("','")
                 .append(tmp.getMesCreacion())
                 .append("','")
                 .append(tmp.getAñoCreacion())
                 .append("','")
                 .append(tmp.getNombre())
                 .append("','")
                 .append(tmp.getRol());
                 
            if(tmp.getHoraCambio() != null){
                query.append("','")
                 .append(tmp.getHoraCambio())
                 .append("','")
                 .append(tmp.getDiaCambio())
                 .append("','")
                 .append(tmp.getMesCambio())
                 .append("','")
                 .append(tmp.getAñoCambio())
                 .append("','")
                 .append(tmp.getUsuarioCambio())
                 .append("'),\n");   
            }else{
                query.append("',")
                 .append(tmp.getHoraCambio())
                 .append(",")
                 .append(tmp.getDiaCambio())
                 .append(",")
                 .append(tmp.getMesCambio())
                 .append(",")
                 .append(tmp.getAñoCambio())
                 .append(",")
                 .append(tmp.getUsuarioCambio())
                 .append("),\n");   
                
            }
                        
        }
        
         File insertSql = new File("sql/insertUsuarios.sql");
         log.info("GENERAO INSERT: \n"+query);
        
        try{
            PrintWriter out= new PrintWriter(insertSql);
            out.println(query);
            
            out.close();
        }catch (FileNotFoundException e){
            log.error("NO SE GENERÓ EL INSERT :"+e);
        
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jtTablaUsuarios = new javax.swing.JTable();

        setTitle("Tabla de Usuarios");

        jtTablaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jtTablaUsuarios);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 976, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmUsersTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmUsersTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmUsersTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmUsersTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmUsersTable().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtTablaUsuarios;
    // End of variables declaration//GEN-END:variables
}
